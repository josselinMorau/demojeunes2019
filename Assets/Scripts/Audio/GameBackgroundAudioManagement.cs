﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBackgroundAudioManagement : PauseBehaviour
{
    /// <summary>
    /// The background audio clip to play.
    /// </summary>
    [SerializeField]
    private AudioClip backgroundAudio;
    
    /// <summary>
    /// Component representing player's health points.
    /// </summary>
    PlayerHealth playerHealth;    

    protected override void Awake()
    {
        base.Awake();
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        if (AudioManager.instance != null) {
            AudioManager.instance.PlayMusicAudioClip(backgroundAudio);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Pause all sounds in the current scene.
    /// </summary>
    protected override void OnPause()
    {
        if (AudioManager.instance != null) {
            AudioManager.instance.Pause();
        }
    }

    /// <summary>
    /// Unpause all sounds in the current scene.
    /// </summary>
    protected override void OnResume()
    {
        if (AudioManager.instance != null) {
            AudioManager.instance.UnPause();
        }
    }
}
