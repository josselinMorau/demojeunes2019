﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    ///<summary>
    /// Audio source that will play music clips.
    ///</summary>
    [SerializeField]
    AudioSource musicSource;

    ///<summary>
    /// Audio source that will play sound effects clips.
    ///</summary>
    [SerializeField]
    AudioSource sfxSource;

    /// <summary>
    /// 
    /// </summary>
    [SerializeField]
    AudioClip mainMenuBackgroundMusic;

    ///<summary>
    /// Minimum pitch range.
    ///</summary>
    [SerializeField]
    float lowPitchRange = .95f;

    ///<summary>
    /// Maximum pitch range.
    ///</summary>
    [SerializeField]
	float highPitchRange = 1.05f;

    ///<summary>
    /// Singleton.
    ///</summary>
    public static AudioManager instance;

    /// <summary>
    /// Music source volume.
    /// </summary>
    public float MusicSoundVolume
    {
        get => musicSource.volume;
        set { musicSource.volume = value; }
    }


    void Awake() 
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Play default music audio clip and loop it.
    /// </summary>
    public void PlayMainMenuMusicBackground()
    {
        musicSource.clip = mainMenuBackgroundMusic;
        musicSource.volume = GlobalSettings.MusicVolume;
        musicSource.Play();
    }

    /// <summary>
    /// Play the given music audio clip and loop it. Only one music can be played at a time. 
    /// </summary>
    /// <param name="clip">Given music clip</param>
    public void PlayMusicAudioClip(AudioClip clip)
    {
        musicSource.clip = clip;
        musicSource.volume = GlobalSettings.MusicVolume;
        musicSource.Play();
    }

    /// <summary>
    /// Restart from the beginning the last music audio clip played. 
    /// </summary>
    public void ReplayMusicAudio() 
    {
        musicSource.Stop();
        musicSource.Play();
    }

    /// <summary>
    /// Play the given sound effect with a little variation of pitch. Many sound effects can be played.
    /// </summary>
    /// <param name="clip">Given sound effect audio clip</param>
    public void PlaySfxAudioClip(AudioClip clip)
    {
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);        
		sfxSource.pitch = randomPitch;
        sfxSource.PlayOneShot(clip, GlobalSettings.SFXVolume);
    }

    /// <summary>
    /// Put music and sound effects clips to pause.
    /// </summary>
    public void Pause() 
    {
        musicSource.Pause();
        sfxSource.Pause();
    }
    
    /// <summary>
    /// Unpause music and sound effects clips in order to continue when they were paused.
    /// </summary>
    public void UnPause() {
        musicSource.UnPause();
        sfxSource.UnPause();
    }

    /// <summary>
    /// Completely stop music and soud effects clips.
    /// </summary>
    public void Stop() 
    {
        musicSource.Stop();
        sfxSource.Stop();
    }
}
