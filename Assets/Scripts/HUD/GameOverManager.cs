using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    /// <summary>
    /// Whether the game is over or not.
    /// </summary>
    public static bool IsGameOver {get; private set;}

    /// <summary>
    /// Reference to the player's health
    /// </summary>
    public PlayerHealth playerHealth;      

    /// <summary>
    /// Time to wait before restarting the level
    /// </summary>
    public float restartDelay = 5f;  

    /// <summary>
    /// Reference to the animator component.
    /// </summary>
    Animator anim;            
    
    /// <summary>
    /// Timer to count up to restarting the level
    /// </summary>
    float restartTimer;                    

    ///<summary>
    /// An array of scripts to disable when the game over starts.
    ///</summary>
    [SerializeField]
    MonoBehaviour[] behavioursToDisable;

    ///<summary>
    /// The animator of the main character.
    ///</summary>
    [SerializeField]
    Animator charAnimator;

    ///<summary>
    /// The death animation's name.
    ///</summary>
    [SerializeField]
    string deathAnimation = "Death";

    /// <summary>
    /// The death audio clip.
    /// </summary>
    [SerializeField]
    AudioClip deathAudio;

    void Awake ()
    {
        // Set up the reference.
        anim = GetComponent<Animator>();
        IsGameOver = false;
    }


    void Update ()
    {
        if(playerHealth.CurrentHealth <= 0 && !IsGameOver)
        {
            StartCoroutine(LaunchGameOver());
        }
    }

    /// <summary>
    /// Launch game over animation and return to the main menu scene.
    /// </summary>
    IEnumerator LaunchGameOver()
    {
        IsGameOver = true;
        AudioManager instance = AudioManager.instance;
        if (instance != null)
        { 
            instance.Stop();
        }
        anim.SetTrigger("GameOver");
        if (charAnimator != null)
        {
            if (instance != null && deathAudio != null)
            {
                AudioManager.instance.PlaySfxAudioClip(deathAudio);
            }
            charAnimator.Play(deathAnimation);
        }

        //disable given scripts
        foreach (var script in behavioursToDisable)
        {
            script.enabled = false;
        }
        //disable scrolling
        GlobalSettings.ScrollingSpeed = 0;
        //reset cursor visibility
        Cursor.visible = true;

        while (restartTimer < restartDelay)
        {
            restartTimer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        SceneManager.LoadScene("HighScoreTable");
        if (instance != null)
        {
            instance.PlayMainMenuMusicBackground();
        }
        
    }
}
