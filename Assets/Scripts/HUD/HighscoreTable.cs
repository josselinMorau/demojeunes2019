﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HighscoreTable : MonoBehaviour
{
    /// <summary>
    /// Time to wait before restarting the level
    /// </summary>
    public float restartDelay = 15.0f;

    /// <summary>
    /// Timer to count up to restarting the level
    /// </summary>
    float restartTimer;

    /// <summary>
    /// Container for each score entry.
    /// </summary>
    private Transform entryContainer;

    /// <summary>
    /// Template for each score entry.
    /// </summary>
    private Transform entryTemplate;

    /// <summary>
    /// List of scores.
    /// </summary>
    private List<Transform> highscoreEntryTransformList;

    /// <summary>
    /// Boolean verifying if the new score entry has been entered by the user or not.
    /// </summary>
    private bool newEntryEntered;

    /// <summary>
    /// Template height to modify the space between each entry.
    /// </summary>
    [SerializeField]
    float templateHeight = 50f;

    private void Awake()
    {
        newEntryEntered = false;
        entryContainer = transform.Find("highscoreEntryContainer");
        entryTemplate = entryContainer.Find("scoreEntryTemplate");
        entryTemplate.gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (newEntryEntered)
        {
            StartCoroutine(ReturnToMainMenuScene());
        }
    }

    public void AddHighscoreEntry(int score, string name)
    {
        HighscoreEntry highscoreEntry = new HighscoreEntry { score = score, name = name };

        string jsonString = PlayerPrefs.GetString("highscoreTable", null);
        Highscores highscores;
        if (jsonString.Equals(""))
        {
            highscores = new Highscores { highscoreEntryList = new List<HighscoreEntry>() };
        } else
        {
            highscores = JsonUtility.FromJson<Highscores>(jsonString);
        }

        highscores.highscoreEntryList.Add(highscoreEntry);
        highscores = SortEntries(highscores);

        string json = JsonUtility.ToJson(highscores);

        PlayerPrefs.SetString("highscoreTable", json);
        PlayerPrefs.Save();
        newEntryEntered = true;
        RefreshHighscoreTable(name);
    }
    
    private void CreateHighscoreEntryTransform(HighscoreEntry highscoreEntry, Transform container, List<Transform> transformList, string playerName = null, int playerRank = 0) 
    {
        Transform entryTransform = Instantiate(entryTemplate, container);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, -templateHeight * transformList.Count);
        entryTransform.gameObject.SetActive(true);
 
        int rank = transformList.Count + 1;
        string rankString;
        switch(rank)
        {
            case 1:
                rankString = "1st";
                break; 
            case 2:
                rankString = "2nd";
                break;
            case 3:
                rankString = "3rd";
                break;
            default:
                if (playerRank != 0)
                {
                    rankString = $"{playerRank}th";
                } else
                {
                    rankString = $"{rank}th";
                }
                break;
        }

        entryTransform.Find("posText").GetComponent<Text>().text = rankString;
        entryTransform.Find("scoreText").GetComponent<Text>().text = highscoreEntry.score.ToString(); 
        entryTransform.Find("nameText").GetComponent<Text>().text = highscoreEntry.name;
        entryTransform.Find("Background").gameObject.SetActive(rank % 2 == 1);

        if (highscoreEntry.name == playerName && highscoreEntry.score == ScoreManager.score && playerRank != 0)
        {
            entryTransform.Find("Background").gameObject.SetActive(true);
            entryTransform.Find("Background").GetComponent<Image>().color = new Color32(199, 213, 116, 255);
        }

        switch (rank)
        {
            case 1:
                break;
            case 2:
                entryTransform.Find("Trophy").GetComponent<Image>().color = new Color32(50, 50, 50, 192);
                break;
            case 3:
                entryTransform.Find("Trophy").GetComponent<Image>().color = new Color32(118, 43, 43, 255);
                break;
            default:
                entryTransform.Find("Trophy").gameObject.SetActive(false);
                break;
        }

        transformList.Add(entryTransform);
    }

    public void RefreshHighscoreTable(string playerName = null)
    {
        string jsonString = PlayerPrefs.GetString("highscoreTable", null);
        if (jsonString == null)
        {
            return;
        }
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);
        if (highscores == null)
        {
            return;
        }

        highscoreEntryTransformList = new List<Transform>();
        int numberOfElementsDisplayed = System.Math.Min(highscores.highscoreEntryList.Count, 10);
        bool isInTopTen = false;
        
        if(playerName != null) {
            int playerScoreIndex = highscores.highscoreEntryList.FindLastIndex(he => he.name == playerName && he.score == ScoreManager.score);
            HighscoreEntry playerScore = highscores.highscoreEntryList[playerScoreIndex];
            for (int i = 0; i < numberOfElementsDisplayed; i++)
            {
                HighscoreEntry highscoreEntry = highscores.highscoreEntryList[i];
                if (i == playerScoreIndex)
                {
                    isInTopTen = true;
                    CreateHighscoreEntryTransform(highscoreEntry, entryContainer, highscoreEntryTransformList, playerName, playerScoreIndex+1);
                } else {
                    CreateHighscoreEntryTransform(highscoreEntry, entryContainer, highscoreEntryTransformList, playerName);
                }
            }

            if (!isInTopTen)
            {
                CreateHighscoreEntryTransform(playerScore, entryContainer, highscoreEntryTransformList, playerName, playerScoreIndex+1);
            }
        }

        

    }

    IEnumerator ReturnToMainMenuScene()
    {
        while (restartTimer < restartDelay)
        {
            restartTimer += Time.deltaTime;
            yield return new WaitForSeconds(1.0f);
        }
        SceneManager.LoadScene("MainMenuScene");
    }

    /// <summary>
    /// Reset saves.
    /// </summary>
    public void ResetData()
    {
        PlayerPrefs.DeleteKey("highscoreTable");
    }

    private Highscores SortEntries(Highscores highscores)
    {
        for (int i = 0; i < highscores.highscoreEntryList.Count; i++)
        {
            for (int j = i + 1; j < highscores.highscoreEntryList.Count; j++)
            {
                if (highscores.highscoreEntryList[j].score > highscores.highscoreEntryList[i].score)
                {
                    HighscoreEntry tmp = highscores.highscoreEntryList[i];
                    highscores.highscoreEntryList[i] = highscores.highscoreEntryList[j];
                    highscores.highscoreEntryList[j] = tmp;
                }
            }
        }
        return highscores;
    }
}
