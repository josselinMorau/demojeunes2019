using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    /// <summary>
    /// Player's score value.
    /// </summary>
    public static int score;

    /// <summary>
    /// Score showed to the user.
    /// </summary>
    Text scoreText;

    private void Awake()
    {
        scoreText = GetComponent<Text>();
        score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score: " + score;
    }
}
