using System.Collections.Generic;

[System.Serializable]
public class Highscores 
{
    public List<HighscoreEntry> highscoreEntryList;
}
