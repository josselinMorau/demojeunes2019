﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_InputWindow : MonoBehaviour
{
    [SerializeField]
    private HighscoreTable highscoreTable;
    private Button okButton;
    private InputField nameInput;

    private void Awake()
    {
        okButton = transform.Find("OkButton").GetComponent<Button>();
        okButton.onClick.AddListener(addNewEntry);
        nameInput = transform.Find("NameInput").GetComponent<InputField>();
        Show();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown("return"))
        {
            addNewEntry();
        }
    }

    public void Show()
    {
        gameObject.SetActive(true);
    } 
    
    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void addNewEntry()
    {
        string playerName = nameInput.text;
        if (playerName.Length != 0)
        {
            Hide();
            highscoreTable.AddHighscoreEntry(ScoreManager.score, playerName);
        }
    }
}
