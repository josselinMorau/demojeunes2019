﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    /// <summary>
    /// Player health at the beginning
    /// </summary>
    [SerializeField]
    private int startingHealth = 10;

    /// <summary>
    /// Player health at time t
    /// </summary>
    private int currentHealth_;

    public int CurrentHealth
    {
        get { return currentHealth_; }
        set
        {
            if (value >= 0)
            {
                currentHealth_ = value;
            }
            else
            {
                currentHealth_ = 0;
            }
        }
    }

    /// <summary>
    /// Slider (HUD element) representing player's health
    /// </summary>
    public Slider healthSlider;

    /// <summary>
    /// Player damage received represented by an image (flash)
    /// </summary>
    public Image damageImage;

    /// <summary>
    /// Speed of the flash when the player receive damage
    /// </summary>
    [SerializeField]
    private float flashSpeed = 5f;

    /// <summary>
    /// Flash color when the player receive damage
    /// </summary>
    [SerializeField]
    private Color flashColour = new Color(1f, 0f, 0f, 0.1f);

     /// <summary>
    /// Player taking damage audio
    /// </summary>
    [SerializeField]
    private AudioClip damageAudio;

    /// <summary>
    /// Player is receiving damage or not
    /// </summary>
    bool damaged;

    private void Awake()
    {
        currentHealth_ = startingHealth;
        healthSlider.maxValue = startingHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if(damaged)
        {
            damageImage.color = flashColour;
        }   
        else
        {
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;
    }

    /// <summary>
    /// Damage taken by the player
    /// </summary>
    /// <param name="amount">The amount of damage taken by the player</param>
    public void TakeDamage (int amount)
    {
        damaged = true;
        currentHealth_ -= amount;
        healthSlider.value = currentHealth_;
        if (AudioManager.instance != null)
        {
            AudioManager.instance.PlaySfxAudioClip(damageAudio);
        }
        if(currentHealth_ <= 0)
        {
            Death();
        }
    }

    /// <summary>
    /// Player is dead
    /// </summary>
    void Death()
    {
        // Game over
    }
}
