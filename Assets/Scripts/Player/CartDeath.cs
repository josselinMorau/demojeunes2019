﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartDeath : MonoBehaviour
{

    ParticleSystem system;

    bool dying;

    // Start is called before the first frame update
    void Start()
    {
        system = GetComponentInChildren<ParticleSystem>();
        dying = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!dying && GameOverManager.IsGameOver)
        {
            StartCoroutine(Explode());
        }
    }

    IEnumerator Explode()
    {
        dying = true;
        if (system != null)
        {
            system.Play();
        }
        foreach (var renderer in GetComponentsInChildren<Renderer>())
        {
            if (!(renderer is ParticleSystemRenderer))
            {
                renderer.enabled = false;
            }
        }
        yield return new WaitForSeconds(system.main.duration);
    }
}
