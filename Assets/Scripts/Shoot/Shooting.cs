﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{

    ///<summary>
    /// The instancied prefab during shooting.
    ///</summary>
    [SerializeField]
    GameObject projectile;

    ///<summary>
    /// The speed at which the projectile is sent.
    ///</summary>
    [SerializeField]
    float speed = 10;

    ///<summary>
    /// The minimum delay between each shot.
    ///</summary>
    [SerializeField]
    float delay = 0.1f;

    ///<summary>
    /// The transform of the origin of the projectiles.
    ///</summary>
    [SerializeField]
    Transform originTransform;

    ///<summary>
    /// The animator of the character.
    ///</summary>
    [SerializeField]
    Animator charAnimator;

    ///<summary>
    /// The shooting animation's name.
    ///</summary>
    [SerializeField]
    string shootAnim = "Shooting";

    /// <summary>
    /// The shooting audio sound clip.
    /// </summary>
    [SerializeField]
    AudioClip shootingAudio;

    float elapsedTime;

    Camera cam;

    Vector3 CursorPosition => GlobalSettings.MotionControlEnabled ? (Vector3)MotionControlCursor.Cursor.Position : Input.mousePosition;

    private void Awake() {
        cam = GetComponentInChildren<Camera>();
        elapsedTime = delay;
    }

    // Update is called once per frame
    void Update()
    {
        if (!PauseManager.pauseManager.IsPaused)
        {
            if (CheckFire())
            {
                elapsedTime = 0;
                GameObject obj = Instantiate(projectile, originTransform.position, Quaternion.identity);
                
                // add force
                Ray ray = cam.ScreenPointToRay(CursorPosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    Vector3 direction = (hit.point - originTransform.position).normalized;
                    obj.GetComponent<Rigidbody>().AddForce(speed * direction);
                    obj.transform.rotation = Quaternion.LookRotation(direction);
                }
                if (charAnimator != null)
                {
                    if (AudioManager.instance != null && shootingAudio)
                    {
                        AudioManager.instance.PlaySfxAudioClip(shootingAudio);
                    }
                    charAnimator.Play(shootAnim);
                }
            }
            else
            {
                elapsedTime += Time.deltaTime;
            }
        }
    }

    ///<summary>
    /// Returns whether or not the condition to shoot is complied.
    ///<summary>
    bool CheckFire()
    {
        if (elapsedTime < delay)
        {
            return false;
        } 
        if (GlobalSettings.MotionControlEnabled)
        {
            return MotionControlCursor.Cursor.IsInScreen;
        }
        return (GlobalSettings.AutoFire && Input.GetButton("Fire1")) || Input.GetButtonDown("Fire1");
    }
}
