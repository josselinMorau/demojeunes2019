﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sight : PauseBehaviour
{

    ///<summary>
    /// The camera where the sight appears.
    ///</summary>
    [SerializeField]
    Camera mainCamera;

    ///<summary>
    /// The distance to the camera of the target in the world.
    ///</summary>
    [SerializeField]
    float distance = 10;

    ///<summary>
    /// The transform of the character model.
    ///</summary>
    [SerializeField]
    Transform characterTransform;

    ///<summary>
    /// The transforms of the character's arms.
    ///</summary>
    [SerializeField]
    Transform[] armTransforms;

    ///<summary>
    /// Additionnal rotations to apply to the arms after orientating it towards the target.
    ///</summary>
    [SerializeField]
    Vector3[] armRotations;

    RectTransform rectTransform;

    protected override void Awake() {
        base.Awake();
        rectTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseManager.pauseManager.IsPaused)
        {
            return;
        }

        Vector2 cursorPos = GetCursorPosition();
        Vector3 pos = mainCamera.ScreenToWorldPoint(new Vector3(cursorPos.x, cursorPos.y, 1));

        rectTransform.position = pos;
    }

    private void LateUpdate() {
        if (PauseManager.pauseManager.IsPaused)
        {
            return;
        }
        
        if (characterTransform != null)
        {
            Vector2 cursorPos = GetCursorPosition();
            if (cursorPos.x > Screen.width ||cursorPos.x < 0 || cursorPos.y > Screen.height || cursorPos.y < 0)
            {
                characterTransform.rotation = Quaternion.identity;
                return;
            }
            Vector3 target = mainCamera.ScreenToWorldPoint(new Vector3(cursorPos.x, cursorPos.y, distance));
            characterTransform.LookAt(new Vector3(target.x, characterTransform.position.y, target.z));

            if (armTransforms != null)
            {
                for (int i=0; i<armTransforms.Length; i++)
                {
                    armTransforms[i].LookAt(target);
                    armTransforms[i].Rotate(armRotations[i]);
                }
            }
        }
    }

    Vector2 GetCursorPosition()
    {
        if (GlobalSettings.MotionControlEnabled)
        {
            return MotionControlCursor.Cursor.Position;
        }
        return Input.mousePosition;
    }

    void OnApplicationFocus(bool focusStatus) {
        if (!PauseManager.pauseManager.IsPaused && focusStatus)
        {
            GlobalSettings.ShowCursor = false;
            Cursor.visible = false;
        }
    }

    void OnDisable() {
        GlobalSettings.ShowCursor = true;
    }

    void OnDetroy() {
        GlobalSettings.ShowCursor = true;
    }

    protected override void OnPause()
    {
        GetComponent<Image>().enabled = false;
        GlobalSettings.ShowCursor = true;
    }

    protected override void OnResume()
    {
        GetComponent<Image>().enabled = true;
        GlobalSettings.ShowCursor = false;
        Cursor.visible = false;
    }
}
