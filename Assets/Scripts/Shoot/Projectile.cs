﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : PauseBehaviour
{

    ///<summary>
    /// The maximum amount of time the projectile stays before being destroyed.
    ///</summary>
    [SerializeField]
    float lifetime;

    ///<summary>
    /// The amount of damage inflicted to the enemy when it hits.
    ///</summary>
    [SerializeField]
    int damage = 1;

    float currentTime;
    bool dying;

    protected override void Awake()
    {
        base.Awake();
        currentTime = 0;
        dying = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!dying)
        {
            if (currentTime >= lifetime)
            {
                Explode();
                return;
            }
            currentTime += Time.deltaTime;
        }
    }

    void OnTriggerEnter(Collider other) {
        GameObject target = other.gameObject;
        if (target.tag == "Enemy")
        {
            Explode(target);
        }
        else if (target.tag != "Player")
        {
            Explode();
        }
    }

    ///<summary>
    /// Destroys the projectile.
    ///</summary>
    void Explode()
    {
        Explode(null);
    }

    ///<summary>
    /// Destroys the projectile and inflict damage to the enemy.
    ///</summary>
    ///<param name="target">The enemy to inflict damage to.</param>
    void Explode(GameObject target)
    {
        ParticleSystem system = GetComponent<ParticleSystem>();
        foreach (var collider in GetComponents<Collider>())
        {
            collider.enabled = false;
        }
        foreach (var renderer in GetComponentsInChildren<Renderer>())
        {
            if (!(renderer is ParticleSystemRenderer))
            {
                renderer.enabled = false;
            }
        }
        Rigidbody rigidbody = GetComponent<Rigidbody>();
        if (rigidbody != null)
        {
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
        }
        dying = true;

        system.Play();

        if (target != null)
        {
            target.GetComponent<EnemyBehavior>().InflictDamage(damage);
        }
        
        StartCoroutine(DestroySelf(system.main.duration));
    }

    ///<summary>
    /// Destroys the object after a given lifetime.
    ///</summary>
    IEnumerator DestroySelf(float duration)
    {
        float elapsedTime = 0;
        while (elapsedTime < duration)
        {
            yield return new WaitForEndOfFrame();
            elapsedTime += Time.deltaTime;
        }
        Destroy(this.gameObject);
    }

    protected override void OnPause()
    {
        ParticleSystem particleSystem = GetComponent<ParticleSystem>();
        if (dying && particleSystem.isPlaying)
        {
            particleSystem.Pause(true);
            particleSystem.Stop(true);
        }
    }

    protected override void OnResume()
    {
        ParticleSystem particleSystem = GetComponent<ParticleSystem>();
        if (dying && particleSystem.isPaused)
        {
            particleSystem.Pause(false);
            particleSystem.Play(true);
        }
    }
}
