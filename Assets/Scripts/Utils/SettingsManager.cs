﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{
    ///<summary>
    /// Checkbox to enable/disable auto-fire.
    ///</summary>
    [SerializeField]
    Toggle autoFireToggle;

    ///<summary>
    /// Checkbox to enable/disable auto-fire.
    ///</summary>
    [SerializeField]
    Toggle motionControlToggle;

    ///<summary>
    /// Slider to edit the master volume.
    ///</summary>
    [SerializeField]
    Slider masterVolumeSlider;

    ///<summary>
    /// Slider to edit the music volume.
    ///</summary>
    [SerializeField]
    Slider musicVolumeSlider;

    ///<summary>
    /// Slider to edit the sounds effects volume.
    ///</summary>
    [SerializeField]
    Slider sfxVolumeSlider;

    ///<summary>
    /// Dropdown to edit the level difficulty of the party.
    ///</summary>
    [SerializeField]
    Dropdown difficultyDropDown;

    ///<summary>
    /// The field for the straffingDistance.
    ///</summary>
    [SerializeField]
    InputField straffingDistanceField;

    ///<summary>
    /// Field to edit the light intensity option.
    ///</summary>
    [SerializeField]
    Slider lightIntensitySlider;

    ///<summary>
    /// The text of bodyAngle.
    ///</summary>
    [SerializeField]
    Toggle rightHandedToggle;

    void Awake()
    {
        
    }

    void OnEnable() {
        // Initialize settings values using GlobalSettings
        if (rightHandedToggle != null)
        {
            rightHandedToggle.SetIsOnWithoutNotify(GlobalSettings.kinectSettings.rightHanded);
        }
        if (straffingDistanceField != null)
        {
            straffingDistanceField.SetTextWithoutNotify(GlobalSettings.StraffingDistance.ToString());
        }
        if (motionControlToggle != null)
        {
            motionControlToggle.isOn = GlobalSettings.MotionControlEnabled;
        }
        if (autoFireToggle != null) 
        {
            autoFireToggle.isOn = GlobalSettings.AutoFire; 
        }
        AudioListener.volume = GlobalSettings.MasterVolume;
        if (masterVolumeSlider != null)
        {
            masterVolumeSlider.value = GlobalSettings.MasterVolume;
        }

        if (musicVolumeSlider != null)
        {
            musicVolumeSlider.value = GlobalSettings.MusicVolume;
        }

        if (sfxVolumeSlider != null)
        {
            sfxVolumeSlider.value = GlobalSettings.SFXVolume;
        }
        if (lightIntensitySlider != null)
        {
            lightIntensitySlider.value = GlobalSettings.LightIntensity;
        }
        if (difficultyDropDown != null)
        {
            switch(GlobalSettings.LevelDifficulty)
            {
                case Difficulty.easy:
                    difficultyDropDown.value = 0;
                    break;
                case Difficulty.normal:
                    difficultyDropDown.value = 1;
                    break;
                case Difficulty.hard:
                    difficultyDropDown.value = 2;
                    break;
                case Difficulty.hell:
                    difficultyDropDown.value = 3;
                    break;
                default:
                    difficultyDropDown.value = 1;
                    break;
            }
        }
    }

    public void UpdateRightHand()
    {
        GlobalSettings.kinectSettings.rightHanded = rightHandedToggle.isOn;
    }

    public void UpdateStraffingDistance()
    {
        float val;
        if (float.TryParse(straffingDistanceField.text, out val))
        {
            GlobalSettings.StraffingDistance = val;
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    ///<summary>
    /// Update auto-fire value in GlobalSettings
    ///</summary>
    public void UpdateAutoFire()
    {
        GlobalSettings.AutoFire = autoFireToggle.isOn;
    }

    ///<summary>
    /// Update motion control enabled value in GlobalSettings
    ///</summary>
    public void UpdateMotionControlEnabled()
    {
        GlobalSettings.MotionControlEnabled = motionControlToggle.isOn;
    }

    ///<summary>
    /// Update master volume in GlobalSettings.
    ///</summary>
    public void UpdateMasterVolume()
    {   
        GlobalSettings.MasterVolume = masterVolumeSlider.value;
        AudioListener.volume = masterVolumeSlider.value;
    }

    ///<summary>
    /// Update music volume in GlobalSettings.
    ///</summary>
    public void UpdateMusicVolume()
    {
        GlobalSettings.MusicVolume = musicVolumeSlider.value;
        if (AudioManager.instance != null) 
        {
            AudioManager.instance.MusicSoundVolume = musicVolumeSlider.value;
        }
    }

    ///<summary>
    /// Update sounds effects volume in GlobalSettings.
    ///</summary>
    public void UpdateSfxVolume()
    {
        GlobalSettings.SFXVolume = sfxVolumeSlider.value;
    } 

    ///<summary>
    /// Update light intensity in GlobalSettings.
    ///</summary>
    public void UpdateLightIntensity()
    {
        GlobalSettings.LightIntensity = lightIntensitySlider.value;
    }
    
    ///<summary>
    /// Update level difficulty of the party.
    ///</summary>
    public void UpdateLevelDifficulty()
    {
        switch(difficultyDropDown.value)
        {
            case 0:
                GlobalSettings.LevelDifficulty = Difficulty.easy;
                break;
            case 1:
                GlobalSettings.LevelDifficulty = Difficulty.normal;
                break;
            case 2:
                GlobalSettings.LevelDifficulty = Difficulty.hard;
                break;
            case 3:
                GlobalSettings.LevelDifficulty = Difficulty.hell;
                break;
        }
    }


}
