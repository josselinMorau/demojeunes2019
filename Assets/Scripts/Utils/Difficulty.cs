﻿///<summary>
/// Defines the game difficulty
///</summary>
public enum Difficulty
{
    easy,
    normal, 
    hard,
    hell 
}
