﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyManager : MonoBehaviour
{
    ///<summary>
    /// Function that will calculate the difficulty through time.
    ///</summary>
    public delegate float DifficultyFunction(float time);

    ///<summary>
    /// A dictionary that associate a function to each difficulty level.
    ///</summary>
    private Dictionary<Difficulty, DifficultyFunction> difficultyFunctions;
    
    ///<summary>
    /// Initial time when the game start.
    ///</summary>
    float startTime;

    ///<summary>
    /// Time t.
    ///</summary>
    float currentTime;

    ///<summary>
    /// The current difficulty progression value.
    ///</summary>
    float currentDifficultyProgression;

    ///<summary>
    /// Smooth value to reduce the acceleration impact of all gameobjects that use ScrollingSpeed.
    ///</summary>
    [SerializeField]
    float smoothAcceleration = 0.5f;

    void Awake() 
    {
        startTime = Time.time;
        difficultyFunctions = new Dictionary<Difficulty, DifficultyFunction>() {
            [Difficulty.easy] = Mathf.Log,
            [Difficulty.normal] = (float time) => time,
            [Difficulty.hard] = (float time) => 2 * time,
            [Difficulty.hell] = (float time) => 5 * time
        };
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameOverManager.IsGameOver)
        {
            return;
        }
        
        currentTime = Time.time - startTime + 1;
        currentDifficultyProgression = difficultyFunctions[GlobalSettings.LevelDifficulty](currentTime);
        if (currentDifficultyProgression < GlobalSettings.DifficultyCap) 
        {
            GlobalSettings.DifficultyProgression = currentDifficultyProgression;
            GlobalSettings.ScrollingSpeed = (GlobalSettings.MaxScrollingSpeed - GlobalSettings.MinScrollingSpeed) * currentDifficultyProgression * smoothAcceleration / GlobalSettings.DifficultyCap + GlobalSettings.MinScrollingSpeed;
        }
        else if (currentDifficultyProgression > GlobalSettings.DifficultyCap)
        {
            GlobalSettings.DifficultyProgression = GlobalSettings.DifficultyCap;
        }

        if (GlobalSettings.ScrollingSpeed > GlobalSettings.MaxScrollingSpeed)
        {
            GlobalSettings.ScrollingSpeed = GlobalSettings.MaxScrollingSpeed;
        }


    }
}
