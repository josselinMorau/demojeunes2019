
///<summary>
/// Container to keep track of the count of any kind of instance.
///</summary>
public class Counter
{

    private int _count;

    ///<summary>
    /// The current count.
    ///</summary>
    public int Count
    {
        get { return _count; }
        set { _count = value >= 0 ? value : 0; }
    }

    ///<summary>
    /// Container to keep track of the count of any kind of instance.
    ///</summary>
    public Counter() : this(0)
    {
    }

    ///<summary>
    /// Container to keep track of the count of any kind of instance.
    ///</summary>
    ///<param name="count">The starting value of the count.</param>
    public Counter(int count)
    {
        this.Count = count;
    }
}