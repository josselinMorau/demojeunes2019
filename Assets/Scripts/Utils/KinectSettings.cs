using System;
using UnityEngine;
using Windows.Kinect;

///<summary>
/// Struct to hold infos about Kinect settings.
///</summary>
public struct KinectSettings
{
    ///<summary>
    /// The position of the center of the screen.
    ///</summary>
    public Vector3 centerPosition;

    ///<summary>
    /// The half of the width of the screen.
    ///</summary>
    public float halfScreenWidth;

    ///<summary>
    /// The half of the height of the screen.
    ///</summary>
    public float halfScreenHeight;

    ///<summary>
    /// Whether or not the right hand is used for the cursor.
    ///</summary>
    public bool rightHanded;

    ///<summary>
    /// Returns the corresponding Vector3 from a CameraSpacePoint. 
    ///</summary>
    public static Vector3 ToVector3(CameraSpacePoint cameraSpacePoint)
    {
        return new Vector3(
            cameraSpacePoint.X,
            cameraSpacePoint.Y,
            cameraSpacePoint.Z
        );
    }

    ///<summary>
    /// Returns the screen position of a tracked hand.
    ///</summary>
    ///<param name="handPosition">The position of the hand.</param>
    ///<param name="bodyPosition">The position of the body.</param>
    public Vector2 GetScreenPosition(Vector3 handPosition, Vector3 bodyPosition)
    {
        float rz1 = Mathf.Abs(bodyPosition.z - handPosition.z) / bodyPosition.z;
        float rz2 = handPosition.z / bodyPosition.z;

        // compute virtual screen size
        Vector2 vHalfSize = rz1 * new Vector2(halfScreenWidth, halfScreenHeight);

        // compute virtual screen position
        Vector2 botL = (Vector2)(centerPosition + rz2 * (bodyPosition - centerPosition)) - vHalfSize;

        // project hand position into screen position
        Vector2 pos = (Vector2)handPosition - botL;
        return new Vector2(pos.x * Screen.width / (2 * vHalfSize.x), pos.y * Screen.height / (2 * vHalfSize.y));
    }
}