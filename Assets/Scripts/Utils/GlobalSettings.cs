using System;
using UnityEngine;

///<summary>
/// Static class containing references of settings for the entire game.
///</summary>
public class GlobalSettings : MonoBehaviour
{

    ///<summary>
    /// Default value for auto-fire option.
    ///</summary>
    private const bool AUTO_FIRE = true;

    ///<summary>
    /// Default value for scrolling speed option.
    ///</summary>
    private const float SCROLLING_SPEED = 1.0f;

    ///<summary>
    /// Default value for scrolling speed option.
    ///</summary>
    private static readonly Vector3 SCROLLING_DIRECTION = new Vector3(0,0,-1);

    ///<summary>
    /// Default value for motion control enabled option.
    ///</summary>
    private const bool MOTION_CONTROL_ENABLED = false;

    ///<summary>
    /// Default value for show cursor option.
    ///</summary>
    private const bool SHOW_CURSOR = true;

    ///<summary>
    /// Default value for straffing distance option.
    ///</summary>
    private const float STRAFFING_DISTANCE = 0.3f;

    ///<summary>
    /// Whether or not the auto-fire is enabled.
    ///</summary>
    public static bool AutoFire {get; set;} = AUTO_FIRE;

    ///<summary>
    /// The global speed of the scrolling.
    ///</summary>
    public static float ScrollingSpeed {get; set;} = SCROLLING_SPEED;

    /// <summary>
    /// Initial value of the global speed of the scrolling.
    /// </summary>
    public static float MinScrollingSpeed { get; set; }

    /// <summary>
    /// Time when accelerating the global speed of the scrolling.
    /// </summary>
    public static float StartAccelerating { get; set; }

    /// <summary>
    /// Cap of the global speed of the scrolling.
    /// </summary>
    public static float MaxScrollingSpeed { get; set; }


    private static Vector3 scrollingDirection_ = SCROLLING_DIRECTION;

    ///<summary>
    /// The global direction of the scrolling.
    ///</summary>
    public static Vector3 ScrollingDirection 
    {
        get => scrollingDirection_;
        set
        {
            scrollingDirection_ = value.normalized;
        }
    }

    ///<summary>
    /// Global kinect settings.
    ///</summary>
    public static KinectSettings kinectSettings = new KinectSettings
    { 
        rightHanded = true,
        halfScreenHeight = 1,
        halfScreenWidth = 1
    };

    private static bool motionControlEnabled_ = MOTION_CONTROL_ENABLED;

    ///<summary>
    /// Whether the game uses motion control or not.
    ///</summary>
    public static bool MotionControlEnabled 
    {
        get => motionControlEnabled_; 
        set
        {
            motionControlEnabled_ = value;
            //Cursor.lockState = value ? CursorLockMode.Locked : CursorLockMode.None;
        }
    }

    public delegate void CursorEventHandler(bool visible);

    ///<summary>
    /// Event triggered when the cursor visibility is switched on or off.
    ///</summary>
    public static event CursorEventHandler switchCursorVisibility;

    private static bool showCursor_ = true;
    ///<summary>
    /// Whether the game cursor is visible or not.
    ///</summary>
    public static bool ShowCursor
    {
        get => showCursor_;
        set
        {
            showCursor_ = value;
            if (switchCursorVisibility != null)
            {
                switchCursorVisibility.Invoke(value);
            }
        }
    }

    private static float straffingDistance_ = STRAFFING_DISTANCE;

    ///<summary>
    /// The distance to trigger straffing (in meter).
    ///</summary>
    public static float StraffingDistance {get => straffingDistance_; set { straffingDistance_ = value > 0 ? value : straffingDistance_;}}

    ///<summary>
    /// Level difficulty of the party.
    ///</summary>
    public static Difficulty LevelDifficulty { get; set; } = Difficulty.normal;

    ///<summary>
    /// Difficulty progression value to calculate the scrolling speed, the enemy frequency.
    ///</summary>
    public static float DifficultyProgression { get; set; } = 1f;

    ///<summary>
    /// Maximal difficulty value.
    ///</summary>
    public static float DifficultyCap { get; set; } = 10f;

    ///<summary>
    /// Master volume equivalent to global volume.
    ///</summary>
    public static float MasterVolume { get; set; } = 1f;

    ///<summary>
    /// Music volume, background volume.
    ///</summary>
    public static float MusicVolume { get; set; } = 1f;

    ///<summary>
    /// Sound effect volume.
    ///</summary>
    public static float SFXVolume { get; set; } = 1f;

    ///<summary>
    /// Light intensity of the main scene.
    ///</summary>
    public static float LightIntensity {get; set;} = 1.25f;

    ///<summary>
    ///The initial speed of the scrolling on awake.
    ///</summary>
    [SerializeField]
    float minScrollingSpeed = SCROLLING_SPEED;

    ///<summary>
    ///The maximum speed of the scrolling.
    ///</summary>
    [SerializeField]
    float maxScrollingSpeed = 60f;

    ///<summary>
    ///The global direction of the scrolling on awake.
    ///</summary>
    [SerializeField]
    Vector3 scrollingDirection = SCROLLING_DIRECTION;

    ///<summary>
    ///The difficulty cap on awake.
    ///</summary>
    [SerializeField]
    float difficultyCap = 100f;

    ///<summary>
    /// Whether the game uses motion control or not.
    ///</summary>
    //[SerializeField]
    //bool motionControlEnabled = MOTION_CONTROL_ENABLED;

    private void Awake() {
        ScrollingSpeed = minScrollingSpeed;
        ScrollingDirection = scrollingDirection;
        DifficultyCap = difficultyCap;
        MinScrollingSpeed = minScrollingSpeed;
        MaxScrollingSpeed = maxScrollingSpeed;
    }

}