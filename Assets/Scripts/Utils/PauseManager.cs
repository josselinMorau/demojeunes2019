using UnityEngine.Events;

///<summary>
/// A singleton class to handle the pause in the game.
///</summary>
public class PauseManager
{

    ///<summary>
    /// The instance of the PauseManager.
    ///</summary>
    public static PauseManager pauseManager = new PauseManager();
    
    UnityEvent mainPauseEvent;

    bool isPaused;

    ///<summary>
    /// The event that handles the pause.
    ///</summary>
    public UnityEvent MainPauseEvent => mainPauseEvent;

    ///<summary>
    /// Whether the game is paused.
    ///</summary>
    public bool IsPaused => isPaused;

    private PauseManager()
    {
        mainPauseEvent = new UnityEvent();
        isPaused = false;
    }
    
    ///<summary>
    /// Pauses the game.
    ///</summary>
    public void Pause()
    {
        isPaused = true;
        mainPauseEvent.Invoke();
    }

    ///<summary>
    /// Resumes the game.
    ///</summary>
    public void Resume()
    {
        isPaused = false;
        mainPauseEvent.Invoke();
    }

}