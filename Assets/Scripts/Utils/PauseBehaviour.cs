﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary>
/// A MonoBehaviour inherited class that implements listeners for the pause.
///</summary>
public class PauseBehaviour : MonoBehaviour
{

    protected virtual void Awake()
    {
        PauseManager.pauseManager.MainPauseEvent.AddListener(OnPauseEventInvoke);
    }

    private void OnPauseEventInvoke()
    {
        if (PauseManager.pauseManager.IsPaused)
        {
            OnPause();
        }
        else
        {
            OnResume();
        }
    }

    ///<summary>
    /// Called whenever the game is paused.
    ///<summary>
    protected virtual void OnPause()
    {
        
    }

    ///<summary>
    /// Called whenever the game resumed.
    ///<summary>
    protected virtual void OnResume()
    {

    }
}
