
///<summary>
/// Defines the type of navigation of an enemy.
///</summary>
public enum NavigationType
{
    chase, // chase the enemy
    obstacle, // spawning obstacles
    rush // rush towards the player position when instanciated and never change its direction
}