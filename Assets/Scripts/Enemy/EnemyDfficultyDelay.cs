using System;

[Serializable]
public struct EnemyDifficultyDelay
{
    ///<summary>
    /// The level difficulty. (easy, normal, hard, hell)
    ///</summary>
    public Difficulty difficulty;

    ///<summary>
    /// Initial delay value.
    ///</summary>
    public float initDelay;
    
    ///<summary>
    /// Maximal delay value.
    ///</summary>
    public float minimumDelay;


}
