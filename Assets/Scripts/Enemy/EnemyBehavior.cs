﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : PauseBehaviour
{
    Transform playerTransform;

    Vector3 targetPosition;

    /// <summary>
    /// Reference to the player's health.
    /// </summary>
    PlayerHealth playerHealth;

    Animator animator;

    /// <summary>
    /// Audio on awake.
    /// </summary>
    [SerializeField]
    AudioClip enemySpawnAudio; 
    
    /// <summary>
    /// Audio on death.
    /// </summary>
    [SerializeField]
    AudioClip enemyDeathAudio;

    ///<summary>
    /// The base rotation of the enemy.
    ///</summary>
    [SerializeField]
    Quaternion baseRotation;

    ///<summary>
    /// Whether the enemey disappears immediatly at death or not.
    ///</summary>
    [SerializeField]
    bool instantDeath = true;

    /// <summary>
    /// Enemy's movement speed.
    /// Whether or not the enemy follows the scrolling movement.
    /// </summary>
    [SerializeField]
    bool useGlobalScrolling = true;

    /// <summary>
    /// The speed of the enemy.
    /// </summary>
    [SerializeField]
    private float movementSpeed = 4.0f;

    /// <summary>
    /// The rotation speed of the enemy.
    /// </summary>
    [SerializeField]
    private float rotationSpeed = 3.0f;

    /// <summary>
    /// Amount of damage the enemy could deal.
    /// </summary>
    [SerializeField]
    private int attackDamage = 1;

    /// <summary>
    /// The value the player can perceive by killing this enemy.
    /// </summary>
    [SerializeField]
    private int scoreValue = 1;

    private int lives_;

    /// <summary>
    /// The current amount of lives of the enemy.
    /// </summary>
    public int Lives
    {
        get { return lives_; }
        set { lives_ = value >= 0 ? value : 0; }
    }

    /// <summary>
    /// The instance counter the Behavior is linked to.
    /// </summary>
    public Counter instanceCounter;

    private NavigationType navType_ = NavigationType.chase;

    /// <summary>
    /// The type of navigation of the enemy.
    /// </summary>
    public NavigationType NavType
    {
        get { return navType_; }
        set { navType_ = value; }
    }

    private bool dying = false;

    protected override void Awake() {
        base.Awake();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        animator = GetComponent<Animator>();
        if (animator != null)
        {
            animator.Play("running");
        }
        if (AudioManager.instance != null && enemySpawnAudio != null && !GameOverManager.IsGameOver)
        {
            AudioManager.instance.PlaySfxAudioClip(enemySpawnAudio);
        }
        transform.rotation = baseRotation;
    }

    void Start()
    {
        if (NavType == NavigationType.rush)
        {
            targetPosition = (playerTransform.position - transform.position).normalized * 
                (useGlobalScrolling ? Mathf.Max(GlobalSettings.ScrollingSpeed, movementSpeed) : movementSpeed);
            RotateTowardsTarget(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!dying && !PauseManager.pauseManager.IsPaused)
        {
            switch (NavType)
            {
                case NavigationType.chase:
                    targetPosition = new Vector3(playerTransform.position.x, transform.position.y, playerTransform.position.z);
                    RotateTowardsTarget();
                    Vector3 v;
                    Vector3 direction = (targetPosition - transform.position).normalized;
                    if (useGlobalScrolling)
                    {
                        v = GlobalSettings.ScrollingDirection * Mathf.Max(movementSpeed, GlobalSettings.ScrollingSpeed)
                            + (direction - GlobalSettings.ScrollingDirection).normalized * movementSpeed;
                    }
                    else
                    {
                        v = movementSpeed * direction;
                    }
                    transform.position += v * Time.deltaTime;
                    break;
                case NavigationType.rush:
                    transform.position += targetPosition * Time.deltaTime;
                    break;
                default:
                    if (useGlobalScrolling)
                    {
                        transform.position += GlobalSettings.ScrollingDirection * GlobalSettings.ScrollingSpeed * Time.deltaTime;
                    }
                    break;
            }

            if (useGlobalScrolling 
                && Vector3.Dot(Camera.main.transform.forward, transform.position - Camera.main.transform.position) < 0)
            {
                // if the scrolling takes away the enemy beyond the camera then it disappears
                Destroy(gameObject);
            }
        }
    }

    /// <summary>
    /// Rotate enemy towards the player.
    /// </summary>
    public void RotateTowardsTarget(bool gradually = true)
    {
        Vector3 newDestination = new Vector3(playerTransform.position.x - transform.position.x, transform.position.y, playerTransform.position.z - transform.position.z);
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(newDestination), rotationSpeed * (gradually ? Time.deltaTime : 1));
    }

    ///<summary>
    /// Inflict damage to enemy and destroy if it has no live left.
    ///</summary>
    ///<param name="damage">The amount of damage taken by the enemy.</param>
    public void InflictDamage(int damage)
    {
        Lives -= damage;
        if (Lives == 0)
        {
            Explode(); // enemy dies
            ScoreManager.score += scoreValue;
        }
        else
        {
            //hit behavior
        }
    }

    /// <summary>
    /// Destroy the enemy.
    /// </summary>
    void Explode()
    {
        Explode(null);
    }

    /// <summary>
    /// Destroy the enemy and inflict damage to the player.
    /// </summary>
    /// <param name="target">The player to inflict damage to.</param>
    void Explode(GameObject target)
    {
        ParticleSystem system = GetComponentInChildren<ParticleSystem>();
        foreach (var collider in GetComponents<Collider>())
        {
            collider.enabled = false;
        }
        dying = true;
        bool hasParticleSystem = system != null;
        bool hasAnimator = animator != null;
        if (AudioManager.instance != null && enemyDeathAudio != null)
        {
            AudioManager.instance.PlaySfxAudioClip(enemyDeathAudio);
        }
        if (instantDeath || target != null)
        {
            if (hasAnimator)
            {
                animator.StopPlayback();
            }
            foreach (var r in GetComponentsInChildren<Renderer>())
            {
                if (!(r is ParticleSystemRenderer))
                {
                    r.enabled = false;
                }
            }
        }
        if (hasParticleSystem && (target != null || instantDeath))
        {
            system.Play();
        }
        else if (hasAnimator)
        {
            animator.Play("dying");
        }

        if (target != null)
        {
            Attack();            
        }

        if (instantDeath || target != null)
        {
            StartCoroutine(DestroySelf(hasParticleSystem ? system.main.duration : 0));
        }
        else
        {
            StartCoroutine(DestroySelf());
        }
    }

    ///<summary>
    /// Destroys the object after a given lifetime.
    ///</summary>
    IEnumerator DestroySelf(float duration)
    {
        float elapsedTime = 0;
        while (elapsedTime < duration)
        {
            yield return new WaitForEndOfFrame();
            elapsedTime += Time.deltaTime;
        }
        Destroy(this.gameObject);
    }

    IEnumerator DestroySelf()
    {
        while (Vector3.Dot(Camera.main.transform.forward, transform.position - Camera.main.transform.position) >= 0)
        {
            transform.position += GlobalSettings.ScrollingDirection * GlobalSettings.ScrollingSpeed * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        Destroy(this.gameObject);
    }

    protected override void OnPause()
    {
        ParticleSystem particleSystem = GetComponentInChildren<ParticleSystem>();
        if (dying && particleSystem.isPlaying)
        {
            particleSystem.Pause(true);
            particleSystem.Stop(true);
        }
    }

    protected override void OnResume()
    {
        ParticleSystem particleSystem = GetComponentInChildren<ParticleSystem>();
        if (dying && particleSystem.isPaused)
        {
            particleSystem.Pause(false);
            particleSystem.Play(true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        GameObject target = other.gameObject;
        if (target.tag == "Player")
        {
            Explode(target);
        }
    }

    private void OnDestroy() {
        if (instanceCounter != null)
        {
            instanceCounter.Count--;
        }
    }

    private void Attack() 
    {
        if (playerHealth.CurrentHealth > 0)
        {
            playerHealth.TakeDamage(attackDamage);
        }
    }
}
