﻿using System;

[Serializable]
public struct EnemyDifficultyCount
{
    ///<summary>
    /// The level difficulty. (easy, normal, hard, hell)
    ///</summary>
    public Difficulty difficulty;

    ///<summary>
    /// Initial number of enemy that will spawn.
    ///</summary>
    public int initCount;

    ///<summary>
    /// The maximal number of enemy that will spawn.
    ///</summary>
    public int maximumCount;
}
