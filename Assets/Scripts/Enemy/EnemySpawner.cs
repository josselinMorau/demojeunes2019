﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

public class EnemySpawner : MonoBehaviour
{
    ///<summary>
    /// The enemy to spawn
    ///</summary>
    public GameObject enemy;

    ///<summary>
    /// The tag used to identify spawn points.
    ///</summary>
    public string spawnTag = "SpawnPoint";

    ///<summary>
    /// The maximum amount of enemies that can appear in during a spawn.
    ///</summary>
    [SerializeField]
    int maxSimultaneousSpawn = 1;

    int simultaneousSpawn;

    /// <summary>
    /// The type of navigation of the spawned enemies.
    /// </summary>
    [SerializeField]
    NavigationType navigationType = NavigationType.chase;

    ///<summary>
    /// The number of lives the enemies have at their appearance.
    ///</summary>
    [SerializeField]
    int lives = 1;

    [SerializeField]
    List<EnemyDifficultyDelay> enemyDifficultyDelay = new List<EnemyDifficultyDelay>() {
        new EnemyDifficultyDelay { difficulty = Difficulty.easy, initDelay = 0f, minimumDelay = 0f },
        new EnemyDifficultyDelay { difficulty = Difficulty.normal, initDelay = 0f, minimumDelay = 0f },
        new EnemyDifficultyDelay { difficulty = Difficulty.hard, initDelay = 0f, minimumDelay = 0f },
        new EnemyDifficultyDelay { difficulty = Difficulty.hell, initDelay = 0f, minimumDelay = 0f },        
    };

    [SerializeField]
    List<EnemyDifficultyCount> enemyDifficultyCount = new List<EnemyDifficultyCount>() {
        new EnemyDifficultyCount { difficulty = Difficulty.easy, initCount = 0, maximumCount = 0 },
        new EnemyDifficultyCount { difficulty = Difficulty.normal, initCount = 0, maximumCount = 0 },
        new EnemyDifficultyCount { difficulty = Difficulty.hard, initCount = 0, maximumCount = 0 },
        new EnemyDifficultyCount { difficulty = Difficulty.hell, initCount = 0, maximumCount = 0 },        
    };

    [SerializeField]
    float startDifficulty;

    EnemyDifficultyCount currentDifficultyCount;
    int enemyCount;    

    EnemyDifficultyDelay currentDifficultyDelay;
    float delay;

    Transform[] spawnPoints;

    public Counter SpawnCounter
    {
        get;
        private set;
    }

    private void Awake() {
        enemyCount = 0;
        delay = 0;
        simultaneousSpawn = 0;

        GameObject[] spawns = GameObject.FindGameObjectsWithTag(spawnTag);
        spawnPoints = new Transform[spawns.Length];
        for ( int i = 0; i < spawns.Length; ++i )
        {
            spawnPoints[i] = spawns[i].transform;
        }
        SpawnCounter = new Counter();
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Spawn());
    }

    void Update() 
    {
        // Update max simultaneous spawn through time.
        simultaneousSpawn = System.Math.Min(enemyCount, maxSimultaneousSpawn);
   
        if (GlobalSettings.DifficultyProgression <= startDifficulty)
        {
            return;
        }

        currentDifficultyCount = enemyDifficultyCount.Find(x => x.difficulty == GlobalSettings.LevelDifficulty);
        enemyCount = (int)((currentDifficultyCount.maximumCount  - currentDifficultyCount.initCount) * GlobalSettings.DifficultyProgression / (GlobalSettings.DifficultyCap - startDifficulty) + currentDifficultyCount.initCount);
        currentDifficultyDelay = enemyDifficultyDelay.Find(x => x.difficulty == GlobalSettings.LevelDifficulty);
        delay = ( currentDifficultyDelay.minimumDelay - currentDifficultyDelay.initDelay) * (GlobalSettings.DifficultyProgression - startDifficulty) / (GlobalSettings.DifficultyCap - startDifficulty) + currentDifficultyDelay.initDelay;
    }

    ///<summary>
    /// Returns whether or not the spawn should end.
    ///</summary>
    bool CheckEnd()
    {
        return false;
    }

    IEnumerator Spawn()
    {
        while (!CheckEnd())
        {
            yield return new WaitForSeconds(delay);
            int spawnNb = Random.Range(0, simultaneousSpawn+1);
            int[] chosenIndexes = new int[spawnNb];
            for (int i=0; i<spawnNb; i++)
            {
                if (SpawnCounter.Count < enemyCount && !PauseManager.pauseManager.IsPaused)
                {
                    int rand = Random.Range(0, spawnPoints.Length);
                    chosenIndexes[i] = rand;
                    bool checkDuplicate = false;
                    for (int j=0; j<i; j++)
                    {
                        if (chosenIndexes[j] == rand)
                        {
                            checkDuplicate = true;
                            break;
                        }
                    }

                    if (!checkDuplicate)
                    {
                        Transform spawnTransform = spawnPoints[rand];
                        GameObject newInstance = Instantiate(enemy, spawnTransform.position + enemy.transform.position, spawnTransform.rotation);
                        SpawnCounter.Count++;
                        EnemyBehavior behavior = newInstance.GetComponent<EnemyBehavior>();
                        behavior.instanceCounter = SpawnCounter;
                        behavior.NavType = navigationType;
                        behavior.Lives = lives;
                    }
                }
                else
                {
                    break;
                }
            }
        }
    }
}
