﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateLight : MonoBehaviour
{

    Light mainLight;

    // Start is called before the first frame update
    void Start()
    {
        mainLight = GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GlobalSettings.LightIntensity != mainLight.intensity)
        {
            mainLight.intensity = GlobalSettings.LightIntensity;
        }
    }
}
