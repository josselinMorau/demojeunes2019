﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingTexture : MonoBehaviour
{
    public float smoothSpeed = 0.1f;

    Renderer rend;

    private void Start() {
        rend = GetComponent<Renderer>();
    }

    private void Update() {
        Vector2 offset = new Vector2(GlobalSettings.ScrollingDirection.x, GlobalSettings.ScrollingDirection.z)
            * GlobalSettings.ScrollingSpeed * Time.deltaTime * smoothSpeed;
        Vector2 normalOffset = rend.material.GetTextureOffset("_BumpMap");
        rend.material.SetTextureOffset("_BumpMap", normalOffset + offset);
        normalOffset = rend.material.GetTextureOffset("_MainTex");
        rend.material.SetTextureOffset("_MainTex", normalOffset + offset);
    }
}
