﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;

public class Straffing : MonoBehaviour
{

    ///<summary>
    /// The speed of the movement.
    ///</summary>
    [SerializeField]
    float speed = 1;

    ///<summary>
    /// The index of the row in which the player start.
    ///</summary>
    [SerializeField]
    int currentRow = 1;

    ///<summary>
    /// The number of rows.
    ///</summary>
    [SerializeField]
    int numberOfRows = 3;

    ///<summary>
    /// Offset between a row and its upper neighbour.
    ///</summary>
    [SerializeField]
    Vector3 offset = Vector3.right;

    ///<summary>
    /// The required offset of position to trigger the straffing when playing with the motion control.
    ///</summary>
    float offsetForMovement;

    bool moving = false;

    BodySourceManager bodySourceManager;

    void Awake() {
        offsetForMovement = GlobalSettings.StraffingDistance;
        if (currentRow >= numberOfRows)
        {
            currentRow = numberOfRows - 1;
        }

        var obj = DontDestroyMainGameCursor.singleton;
        if (obj != null)
        {
            bodySourceManager = obj.GetComponentInChildren<BodySourceManager>();
        }
    }

    float GetHorizontalMovement()
    {
        if (GlobalSettings.MotionControlEnabled)
        {
            if (bodySourceManager != null)
            {
                Body body = bodySourceManager.GetMainBody();
                if (body != null && body.IsTracked)
                {
                    float x = body.Joints[JointType.SpineBase].Position.X;
                    float cx = GlobalSettings.kinectSettings.centerPosition.x + (currentRow - 1) * 2 * offsetForMovement;
                    return x > cx + offsetForMovement ? 1 : (x < cx - offsetForMovement ? -1 : 0);
                }
            }
            return 0;
        }
        return Input.GetAxisRaw("Horizontal");
    }

    // Update is called once per frame
    void Update()
    {
        if (!moving && !PauseManager.pauseManager.IsPaused)
        {
            float axisVal = GetHorizontalMovement();
            if (axisVal < 0 && currentRow > 0)
            {
                StartCoroutine(MoveTo(transform.position, transform.position - offset));
                currentRow--;
            }
            else if (axisVal > 0 && currentRow < numberOfRows - 1)
            {
                StartCoroutine(MoveTo(transform.position, transform.position + offset));
                currentRow++;
            }
        }
    }

    IEnumerator MoveTo(Vector3 start, Vector3 target)
    {
        moving = true;
        Vector3 direction = target - start;
        direction.Normalize();
        while (Vector3.Dot(target - transform.position, direction) > 0)
        {
            if (!PauseManager.pauseManager.IsPaused)
            {
                transform.Translate(speed * Time.deltaTime * direction);
                yield return new WaitForSeconds(Time.deltaTime);
            }
            else
            {
                yield return new WaitForEndOfFrame();
            }
        }
        transform.SetPositionAndRotation(target, transform.rotation);
        moving = false;
        yield return null;
    }
}
