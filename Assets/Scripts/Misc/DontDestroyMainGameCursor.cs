﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyMainGameCursor : MonoBehaviour
{
    public static DontDestroyMainGameCursor singleton;

    void Awake()
    {
        if (singleton == null)
        {
            singleton = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
