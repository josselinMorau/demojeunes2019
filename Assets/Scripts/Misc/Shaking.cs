﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shaking : MonoBehaviour
{
    ///<sumary>
    /// The amplitude of the shaking.
    ///</summary>
    [SerializeField]
    float amplitude = 0;

    ///<sumary>
    /// The periode of the shaking.
    ///</summary>
    [SerializeField]
    float periode = 1;
    
    ///<sumary>
    /// The direction of the shaking.
    ///</summary>
    [SerializeField]
    Vector3 direction = Vector3.up;

    float pulsation;

    Vector3 inititalPos;

    private void Awake() {
        pulsation = 2 * Mathf.PI / periode;
        inititalPos = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if (!PauseManager.pauseManager.IsPaused)
        {
            transform.localPosition = inititalPos + amplitude * Mathf.Sin(pulsation * Time.time) * direction;
        }
    }
}
