﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeGenerator : MonoBehaviour
{

    ///<summary>
    /// The prefab object to generate.
    ///</summary>
    [SerializeField]
    GameObject prefab;

    ///<summary>
    /// The position of the first generated instance relative to the current Transform.
    ///</summary>
    [SerializeField]
    Vector3 origin;

    ///<summary>
    /// The offset between two generated instances.
    ///</summary>
    [SerializeField]
    Vector3 offset = Vector3.forward;

    ///<summary>
    /// The number of instances to generate.
    ///</summary>
    [SerializeField]
    uint count;

    void Awake()
    {
        Vector3 position = transform.position + origin;
        for (int i=0; i<count; i++)
        {
            Instantiate(prefab, position, Quaternion.identity, transform);
            position += offset;
        }
    }
}
