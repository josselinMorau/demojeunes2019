﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugCursor : MonoBehaviour
{

    [SerializeField]
    float ratio = 1;

    RectTransform rectTransform;

    Vector2 originalPos;

    // Start is called before the first frame update
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        originalPos = rectTransform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (MotionControlCursor.Cursor != null)
        {
            if (MotionControlCursor.Cursor.IsInScreen)
                rectTransform.position = originalPos + MotionControlCursor.Cursor.Position * ratio;
        }
    }
}
