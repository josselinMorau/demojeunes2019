﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{

    [SerializeField]
    ///<summary>
    /// Whether or not the scrolling use the values described in GlobalSettings.
    ///</summary>
    bool useGlobalScrolling = true;

    [SerializeField]
    ///<summary>
    /// The distance between the goal and the departure.
    ///</summary>
    float distance;

    [SerializeField]
    ///<summary>
    /// The direction of the scrolling. Only used if useGlobalScrolling is set to false.
    ///</summary>
    Vector3 scrollingDirection;

    [SerializeField]
    ///<summary>
    /// The speed of the scrolling.
    /// If the global settings are used, this value is multiplied with the global scrolling speed.
    ///</summary>
    float speed = 1.0f;

    ///<summary>
    /// The speed of the scrolling.
    /// If the global settings are used, this value is multiplied with the global scrolling speed.
    ///</summary>
    public float Speed
    {
        get => useGlobalScrolling ? speed * GlobalSettings.ScrollingSpeed : speed;
        set { speed = value; }
    }

    private Transform[] movingTransforms;

    Vector3 absoluteGoal;

    void Start()
    {
        if (useGlobalScrolling)
        {
            scrollingDirection = GlobalSettings.ScrollingDirection;
        }
        else
        {
            scrollingDirection.Normalize();
        }

        float halfDistance = distance / 2;
        absoluteGoal = transform.position + halfDistance * scrollingDirection;

        movingTransforms = new Transform[transform.childCount];
        for (int i=0; i < transform.childCount; i++)
        {
            movingTransforms[i] = transform.GetChild(i);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseManager.pauseManager.IsPaused || Speed == 0)
        {
            return;
        }
        
        foreach (Transform trf in movingTransforms)
        {
            trf.position += scrollingDirection * Speed * Time.deltaTime;
            // Check if transform has crossed the absoluteGoal position.
            if (Vector3.Dot(absoluteGoal - trf.position, scrollingDirection) < 0)
            {
                // set the object's position to the beginning of the scrolling
                trf.position -= distance * scrollingDirection;
            }
        }
    }
}
