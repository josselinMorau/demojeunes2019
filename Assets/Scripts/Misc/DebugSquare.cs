﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugSquare : MonoBehaviour
{
    ///<summary>
    /// The ratio of the square.
    ///</summary>
    [SerializeField]
    float ratio = 0.01f;

    ///<summary>
    /// The image of the mini cursor.
    ///</summary>
    [SerializeField]
    Image miniCursor;

    ///<summary>
    /// The image of the square.
    ///</summary>
    [SerializeField]
    Image debugRect;

    // Start is called before the first frame update
    void Start()
    {
        if (debugRect != null)
        {
            debugRect.rectTransform.sizeDelta = new Vector2(Screen.width, Screen.height) * ratio;
        }
        if (miniCursor != null)
        {
            miniCursor.rectTransform.sizeDelta *= ratio;
        }
    }
    
}
