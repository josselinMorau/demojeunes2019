﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Windows.Kinect;

public class FollowCursorPosition : MonoBehaviour
{
    [SerializeField]
    bool useMotion = false;

    Camera mainCamera;

    RectTransform rectTransform;
    
    Image cursorImage;

    bool visible;

    ///<summary>
    /// Whether the cursor moves freely or not.
    ///</summary>
    public bool FreeMovement { get; set; }

    ///<summary>
    /// The current position of the cursor. Can only bet set if FreeMovement is set to false.
    ///</summary>
    public Vector2 Position
    {
        get => rectTransform.position;
        set
        {
            if (!FreeMovement)
            {
                rectTransform.position = new Vector3(value.x, value.y, rectTransform.position.z);
            }
        }
    }

    void Awake()
    {
        FreeMovement = true;
        Cursor.visible = false;
        SceneManager.sceneLoaded += OnSceneLoaded;
        mainCamera = Camera.main;
        visible = true;
        cursorImage = GetComponent<Image>();
        rectTransform = GetComponent<RectTransform>();
        GlobalSettings.switchCursorVisibility += OnVisibilitySwitch;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        mainCamera = Camera.main;
    }

    private void OnApplicationFocus(bool focusStatus) {
        if (focusStatus)
        {
            Cursor.visible = false;
        }
    }

    void OnVisibilitySwitch(bool visible)
    {
        if (cursorImage != null)
        {
            cursorImage.enabled = visible;
            this.visible = visible;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!visible || !FreeMovement)
        {
            return;
        }

        rectTransform.position = GetCursorPosition();
    }
    
    Vector2 GetCursorPosition()
    {
        if (GlobalSettings.MotionControlEnabled && useMotion)
        {
            return MotionControlCursor.Cursor.Position;
        }
        return Input.mousePosition;
    }
}
