using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour
{
    /// <summary>
    /// Load main game scene.
    /// </summary>
    public void LoadGameScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

}
