﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    /// <summary>
    /// Game status (paused or not)
    /// </summary>
    public static bool gameIsPaused = false;

    /// <summary>
    /// Pause Menu Interface
    /// </summary>
    public GameObject pauseMenuUI;
    

    void Awake()
    {
        Resume();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameIsPaused)
            {
                Resume();
            } else
            {
                Pause();
            }
        }
    }

    /// <summary>
    /// Get back to the game
    /// </summary>
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
        PauseManager.pauseManager.Resume();
    }

    /// <summary>
    /// Pause the game
    /// </summary>
    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
        PauseManager.pauseManager.Pause();
    }

    /// <summary>
    /// Restart the game
    /// </summary>
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        if (AudioManager.instance != null) 
        {
            AudioManager.instance.ReplayMusicAudio();
        }
    }

    /// <summary>
    /// Return to main menu screen
    /// </summary>
    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("MainMenuScene");
        if (AudioManager.instance != null) 
        {
            AudioManager.instance.PlayMainMenuMusicBackground();
        }
    }
}
