﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;

public class MotionControlCursor : MonoBehaviour
{
    ///<summary>
    /// Singleton instance of the cursor.
    ///</summary>
    public static MotionControlCursor Cursor {get; private set;}

    ///<summary>
    /// Reference to the script that gathers data from the tracked body.
    ///</summary>
    [SerializeField]
    BodySourceManager bodySourceManager;

    ///<summary>
    /// The minimum amount of frames where the player needs to keep his hand closed to 'click' on screen.
    ///</summary>
    [SerializeField]
    int minHandClickCount = 120;

    ///<summary>
    /// The current position of the cursor.
    ///</summary>
    public Vector2 Position {get; private set;}

    ///<summary>
    /// Whether the associated joint is currently tracked.
    ///</summary>
    public bool IsDetecting {get; private set;}

    public HandState TrackedHandState {get; private set;} = HandState.Unknown;

    int handClickCount_ = 0;

    int HandClickCount
    {
        get => handClickCount_;
        set { handClickCount_ = value > 0 ? (value <= minHandClickCount ? value : minHandClickCount) : 0; }
    }

    ///<summary>
    /// Whether the player currently has the hand in the closed 'clicking' state.
    ///</summary>
    public bool IsClicking {get; private set;} = false;

    ///<summary>
    /// Whether the cursor is in the screen or not.
    ///</summary>
    public bool IsInScreen
    {
        get
        {
            float x = Position.x;
            float y = Position.y;
            return x >= 0 && x <= Screen.width && y >= 0 && y <= Screen.height;
        }
    }

    public delegate void MotionClickEventHandler();

    ///<summary>
    /// Event triggered whenever the player enters the 'click' position with his hand.
    ///</summary>
    public event MotionClickEventHandler OnMotionClickEnter;

    ///<summary>
    /// Event triggered whenever the player exits the 'click' position with his hand.
    ///</summary>
    public event MotionClickEventHandler OnMotionClickExit;

    void Awake() {
        if (Cursor == null)
        {
            Cursor = this;
        }
        IsDetecting = false;
    }

    // Update is called once per frame
    void Update()
    {
        IsDetecting = false;
        if (bodySourceManager == null)
        {
            TrackedHandState = HandState.NotTracked;
            return;
        }

        Body body = bodySourceManager.GetMainBody();
        if (body == null || !body.IsTracked)
        {
            TrackedHandState = HandState.NotTracked;
            return;
        }
        bool rightHanded = GlobalSettings.kinectSettings.rightHanded;
        HandState handState = rightHanded ? body.HandRightState : body.HandLeftState;

        // check if the player is 'clicking'
        if (handState == HandState.Closed)
        {
            HandClickCount++;
        }
        else if (handState == HandState.Open)
        {
            HandClickCount--;
        }

        if (!IsClicking && HandClickCount == minHandClickCount)
        {
            IsClicking = true;
            if (OnMotionClickEnter != null)
            {
                OnMotionClickEnter.Invoke();
            }
        }
        else if (IsClicking && HandClickCount == 0)
        {
            IsClicking = false;
            if (OnMotionClickExit != null)
            {
                OnMotionClickExit.Invoke();
            }
        }

        TrackedHandState = handState;
        JointType handJointType = rightHanded ? JointType.HandRight : JointType.HandLeft;
        Vector3 handPosition = KinectSettings.ToVector3(body.Joints[handJointType].Position);
        Vector3 bodyPosition = KinectSettings.ToVector3(body.Joints[JointType.SpineShoulder].Position);
        Position = GlobalSettings.kinectSettings.GetScreenPosition(handPosition, bodyPosition);
        IsDetecting = true;
        return;
    }
}
