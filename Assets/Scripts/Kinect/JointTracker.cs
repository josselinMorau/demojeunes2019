﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;

public class JointTracker : MonoBehaviour
{
    ///<summary>
    /// Reference to the script that gathers data from the tracked body.
    ///</summary>
    [SerializeField]
    BodySourceManager bodySourceManager;

    BodySourceManager BodySourceManager
    {
        get => bodySourceManager;
        set { bodySourceManager = value; }
    }

    ///<summary>
    /// The type of joint to track.
    ///</summary>
    [SerializeField]
    JointType jointType;

    ///<summary>
    /// The current position of the cursor.
    ///</summary>
    public Vector3 Position {get; private set;}

    ///<summary>
    /// Whether the associated joint is currently tracked.
    ///</summary>
    public bool IsDetecting {get; private set;}

    ///<summary>
    /// Setup variables of JointTracker.
    ///</summary>
    public void SetValues(BodySourceManager sourceManager, JointType jointType)
    {
        bodySourceManager = sourceManager;
        this.jointType = jointType;
    }

    // Start is called before the first frame update
    void Start()
    {
        IsDetecting = false;
    }

    // Update is called once per frame
    void Update()
    {
        IsDetecting = false;
        if (bodySourceManager == null)
        {
            return;
        }

        Body body = bodySourceManager.GetMainBody();
        if (body != null || body.IsTracked)
        {
            Position = KinectSettings.ToVector3(body.Joints[jointType].Position);
            IsDetecting = true;
        }
    }
}
