﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MotionControlMenu : MonoBehaviour
{
    ///<summary>
    /// The buttons of the menu.
    ///</summary>
    [SerializeField]
    Button[] buttons;

    ///<summary>
    /// The buttons of the menu.
    ///</summary>
    Button[] Buttons
    {
        get => buttons;
        set { buttons = value; }
    }

    FollowCursorPosition followCursor;

    int selected = 0;

    int pressed = -1;

    private void Start() {
        followCursor = DontDestroyMainGameCursor.singleton.GetComponentInChildren<FollowCursorPosition>();
        Subscribe();
    }

    private void OnDestroy() {
        Unsubscribe();
    }

    private void OnEnable() {
        Subscribe();
        if (followCursor == null)
        {
            DontDestroyMainGameCursor mainGameCursor = DontDestroyMainGameCursor.singleton;
            if (mainGameCursor != null)
            {
                followCursor = mainGameCursor.gameObject.GetComponentInChildren<FollowCursorPosition>();
            }
        }
    }

    private void OnDisable() {
        Unsubscribe();
    }

    void Subscribe()
    {
        if (MotionControlCursor.Cursor != null)
        {
            MotionControlCursor.Cursor.OnMotionClickEnter += OnClickEnter;
            MotionControlCursor.Cursor.OnMotionClickExit += OnClickExit;
        }
    }

    void Unsubscribe()
    {
        if (MotionControlCursor.Cursor != null)
        {
            MotionControlCursor.Cursor.OnMotionClickEnter -= OnClickEnter;
            MotionControlCursor.Cursor.OnMotionClickExit -= OnClickExit;
        }
    }

    void OnClickEnter()
    {
        if (!GlobalSettings.MotionControlEnabled)
        {
            return;
        }
        pressed = selected;
        if (pressed < buttons.Length)
        {
            buttons[pressed].OnPointerDown(new PointerEventData(EventSystem.current));
        }
    }

    void OnClickExit()
    {
        if (!GlobalSettings.MotionControlEnabled)
        {
            return;
        }
        if (pressed < buttons.Length)
        {
            buttons[pressed].OnPointerUp(new PointerEventData(EventSystem.current));
            if (pressed == selected)
            {
                buttons[pressed].onClick.Invoke();
            }
        }
    }

    private void Update() {
        if (!GlobalSettings.MotionControlEnabled)
        {
            followCursor.FreeMovement = true;
            return;
        }
        followCursor.FreeMovement = false;

        int size = buttons.Length;
        if (size == 0)
        {
            return;
        }

        float buttonHeight = Screen.height / size;
        float currentHeight = MotionControlCursor.Cursor.Position.y;
        for (int i=0; i<size; i++)
        {
            if (currentHeight <= buttonHeight * (i+1))
            {
                if (selected != i)
                {
                    selected = size - i - 1;
                    Button selectedButton = buttons[selected];
                    selectedButton.Select();
                    followCursor.Position = selectedButton.GetComponent<RectTransform>().position;
                }
                break;
            }
        }
    }
}
