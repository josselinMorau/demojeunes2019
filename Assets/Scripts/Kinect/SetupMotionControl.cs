﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Windows.Kinect;

public class SetupMotionControl : PauseBehaviour
{

    enum SetupState
    {
        init,
        hand, // setups which hand to use
        bottomLeft, // setups the bottom-left corner
        topRight,// setups the top-right corner
        finished,
        error
    }

    ///<summary>
    /// Reference to the script that gathers data from the tracked body.
    ///</summary>
    BodySourceManager bodySourceManager;

    ///<summary>
    /// The margin error of the joints position.
    ///</summary>
    [SerializeField]
    float marginError = 0.01f;

    ///<summary>
    /// The number of seconds to wait between each setup state.
    ///</summary>
    [SerializeField]
    float timeBetweenState = 2;

    ///<summary>
    /// The object to aim to compute the position of the bottom-left corner of the screen.
    ///</summary>
    [SerializeField]
    GameObject bottomLeftCorner;

    ///<summary>
    /// The object to aim to compute the position of the top-right corner of the screen.
    ///</summary>
    [SerializeField]
    GameObject topRightCorner;

    Text instructionText;

    SetupState state;

    Body mainBody;

    JointTracker handTracker;

    JointTracker bodyTracker;

    bool acquisitionTime_ = false;

    bool AcquisitionTime
    {
        get => acquisitionTime_;
        set
        {
            acquisitionTime_ = value;
            if (!value)
            {
                click = false;
            }
        }
    }

    bool click = false;

    Vector3 bottomLeftCornerPos;

    Vector3 topRightCornerPos;

    Vector3 playerPos;
    
    // Start is called before the first frame update
    void Start()
    {
        GlobalSettings.ShowCursor = true;
        try
        {
            bodySourceManager = GameObject.FindGameObjectWithTag("BodySourceManager").GetComponent<BodySourceManager>();
        }
        catch
        {
            bodySourceManager = null;
        }
        instructionText = GetComponentInChildren<Text>();
        if (bodySourceManager == null || !bodySourceManager.Active)
        {
            state = SetupState.error;
        }
        else
        {
            state = SetupState.init;
        }
        MotionControlCursor.Cursor.OnMotionClickEnter += ClickEnter;
        MotionControlCursor.Cursor.OnMotionClickExit += ClickExit;
        StartNextCoroutine();
    }

    
    public void ClickEnter()
    {
        if (AcquisitionTime)
        {
            click = true;
        }
    }

    public void ClickExit()
    {
        click = false;
    }

    protected override void OnResume()
    {
        GlobalSettings.ShowCursor = true;
    }

    protected override void OnPause()
    {
        GlobalSettings.ShowCursor = true;
    }

    IEnumerator Init()
    {
        instructionText.text = "Patientez...";
        yield return new WaitForSeconds(timeBetweenState);
        instructionText.text = "Placez vous bien au milieu de l'écran. Les bras baissés.";
        while (mainBody == null)
        {
            Body[] bodies = bodySourceManager.GetData();
            if (bodies != null)
            {
                foreach (var body in bodies)
                {
                    if (body.IsTracked)
                    {
                        mainBody = body;
                        break;
                    }
                }
            }
            yield return new WaitForEndOfFrame();
        }

        instructionText.text = "Restez au milieu de l'écran pendant quelques secondes";
        yield return new WaitForSeconds(timeBetweenState);

        if (mainBody != null && mainBody.IsTracked)
        {
            playerPos = KinectSettings.ToVector3(mainBody.Joints[JointType.SpineShoulder].Position);
            state++;
            instructionText.text = "Patientez...";
            yield return new WaitForSeconds(timeBetweenState);
        }
        else
        {
            state = SetupState.error;
        }
        StartNextCoroutine();
    }

    IEnumerator Hand()
    {
        instructionText.text = "Levez la main que vous voulez utiliser au niveau de votre tête.";
        string str = "";
        while (true)
        {
            Windows.Kinect.Joint leftHand = mainBody.Joints[JointType.HandLeft];
            Windows.Kinect.Joint rightHand = mainBody.Joints[JointType.HandRight];
            Windows.Kinect.Joint head = mainBody.Joints[JointType.Head];
            if (leftHand.Position.Y >= head.Position.Y - marginError)
            {
                str = "gauche";
                GlobalSettings.kinectSettings.rightHanded = false;
                break;
            }
            if (rightHand.Position.Y >= head.Position.Y - marginError)
            {
                str = "droite";
                GlobalSettings.kinectSettings.rightHanded = true;
                break;
            }
            yield return new WaitForEndOfFrame();
        }
        instructionText.text = $"Vous avez choisi la main {str}. Veuillez patienter...";
        yield return new WaitForSeconds(timeBetweenState);
        state++;
        StartNextCoroutine();
    }

    IEnumerator Corner(bool bottomLeft)
    {   
        GameObject corner = bottomLeft ? bottomLeftCorner : topRightCorner;
        if (corner != null)
        {
            corner.SetActive(true);
        }
        if (handTracker == null)
        {
            handTracker = gameObject.AddComponent<JointTracker>();
            handTracker.SetValues(bodySourceManager, 
                GlobalSettings.kinectSettings.rightHanded ? JointType.HandRight : JointType.HandLeft);
        }
        if (bodyTracker == null)
        {
            bodyTracker = gameObject.AddComponent<JointTracker>();
            bodyTracker.SetValues(bodySourceManager, JointType.SpineShoulder);
        }
        
        instructionText.text = "Tendez votre main vers la cible puis maintenez le poing fermé pour confirmer.";
        AcquisitionTime = true;
        while (true)
        {
            if (click)
            {
                if (bottomLeft)
                {
                    bottomLeftCornerPos = handTracker.Position;
                }
                else
                {
                    topRightCornerPos = handTracker.Position;
                }
                break;
            }
            yield return new WaitForEndOfFrame();
        }
        AcquisitionTime = false;
        corner.SetActive(false);
        instructionText.text = "Veuillez patienter...";
        yield return new WaitForSeconds(timeBetweenState);

        if (!bottomLeft)
        {
            if (bottomLeftCornerPos.x >= topRightCornerPos.x || bottomLeftCornerPos.y >= topRightCornerPos.y)
            {
                state = SetupState.error;
            }
            else
            {
                float rz = playerPos.z / (playerPos.z - topRightCornerPos.z);
                GlobalSettings.kinectSettings.halfScreenWidth = Mathf.Abs(topRightCornerPos.x - playerPos.x) * rz;
                float bottomY = playerPos.y + playerPos.z * (bottomLeftCornerPos.y - playerPos.y) / Mathf.Abs(bottomLeftCornerPos.z - playerPos.z);
                float topY = playerPos.y + playerPos.z * (topRightCornerPos.y - playerPos.y) / Mathf.Abs(topRightCornerPos.z - playerPos.z);
                GlobalSettings.kinectSettings.halfScreenHeight = (topY - bottomY) / 2f;
                GlobalSettings.kinectSettings.centerPosition = new Vector3(playerPos.x, bottomY + GlobalSettings.kinectSettings.halfScreenHeight, 0);
                float y = bodySourceManager.GetMainBody().Joints[JointType.FootLeft].Position.Y;
            }
        }
        if (state != SetupState.error)
        {
            state++;
        }
        StartNextCoroutine();
    }

    IEnumerator Finish()
    {
        GlobalSettings.ShowCursor = true;
        instructionText.text = "Configuration terminée.";
        yield return new WaitForSeconds(timeBetweenState);
        SceneManager.LoadScene("MainMenuScene");
    }

    IEnumerator Error()
    {
        GlobalSettings.ShowCursor = true;
        instructionText.text = "Une erreur est survenue lors de la configuration.";
        yield return new WaitForSeconds(timeBetweenState);
        SceneManager.LoadScene("MainMenuScene");
    }

    void StartNextCoroutine()
    {
        switch (state)
        {
            case SetupState.init:
                StartCoroutine(Init());
                break;
            case SetupState.hand:
                StartCoroutine(Hand());
                break;
            case SetupState.bottomLeft:
                StartCoroutine(Corner(true));
                break;
            case SetupState.topRight:
                StartCoroutine(Corner(false));
                break;
            case SetupState.finished:
                StartCoroutine(Finish());
                break;
            case SetupState.error:
                StartCoroutine(Error());
                break;
        }
    }
}
