# À propos de Gitflow

Gitflow est le nom d'une méthode à appliquer sur un projet de développement qui utilise l'outil de versionnage Git.
Ce document sert d'introduction et de rappel du principe de cette méthode ainsi que de bonnes pratiques lors de l'utilisation de Git.
Il ne s'agit pas d'un tutoriel sur Git. Cherchez sur [la documentation officielle de Git](https://git-scm.com/doc) ou n'importe quel tutoriel sur le net si jamais vous rencontrez des difficultés avec les lignes de commande ou le fonctionnement général de Git.
Si jamais vous voulez plus en savoir sur Gitflow, je vous invite à lire le [post original](https://nvie.com/posts/a-successful-git-branching-model/) du créateur de la méthode.

## Sommaire

- Qu'est-ce que Gitflow ?
- Les différentes branches
- Comment travailler sur sa branche ?
- Quelques bonnes pratiques
    - Revue de code
    - Nommer ses commits
    - Faire des commits réguliers
    - Utiliser les issues
- Résumé du workflow
    - Avant de commencer une tâche
    - Une fois la tâche terminée
    - À ne pas faire !

## Qu'est-ce que Gitflow ?

Gitflow est un worklow ou en français "flux de travail" (mais soyons honnêtes personne ne dit ça). Brièvement, il s'agit d'une série de tâches et de bonnes pratiques à adopter pour organiser son travail. En ce qui nous concerne, Gitflow décrit une façon d'organiser les branches de son repertoire Git et la façon de travailler sur ces dernières.
Il existe de nombreux outils pour forcer le comportement prévu par Gitflow sur un répertoire Git (dont on se passera mais ça vous empêche pas d'y jeter un oeil).
Gardez à l'esprit qu'il s'agit ici d'une méthode de travail et d'organisation et en aucun cas une loi sacrée et inviolable. Libre à vous d'adapter la méthode à vos besoins et à l'envergure de votre projet (voire de l'ignorer mais bon on va partir du principe que vous l'appliquez quand même; ce serait sympa).

## Les différentes branches

Gitlow décrit 5 types de branches pour un projet:

- La branche "master", la branche dite de production. C'est la plus importante. Celle qu'il ne faut à tout prix pas casser. C'est celle-ci qui représentera le statut actuel de votre projet publié. En prenant l'example d'un site web, ce sera le code utilisé par la version actuellement déployée de votre site. Sur les projets openSource, c'est celle que l'on garde publique afin que les utilisateurs soient libres de chercher où se trouve le problème si jamais ils rencontrent un bug. Il est important de noter que les développeurs ne doivent **PAS** travailler directement sur cette branche (sous peine de se faire méchamment fouetter par le propriétaire du projet) pour éviter de casser le code actuellement en production.

- La branche "develop", celle dite de développement. C'est la branche sur laquelle les développeurs font avancer les futures fonctionnalités à ajouter. Il est conseillé d'en faire la branche par défaut d'un projet dans le sens où toutes les Merge Requests se font par défaut vers cette branche. Encore une fois, il ne vaut mieux pas développer directement sur cette branche à moins qu'il ne s'agisse de changements minimes se réglant en un seul petit commit ou si vous êtes le seul développeur sur le répertoire (mais dans ce cas pas sûr que Gitflow serve vraiment ici...).

- Les branches "feature", celles dites de fonctionnalités. Ce sont celles sur lesquelles les développeurs vont travailler sur les nouvelles fonctionnalités ou simplement sur une correction de problème ou "issue" comme des bugs. Lorsqu'un développeur se voit attribué une tâche, il créé une nouvelle branche feature à partir de la version la plus récente de la branche develop. Une fois celle-ci terminée, il la fusionne avec la branche develop puis créé une nouvelle pour sa prochaine tâche et ainsi de suite. Fonctionner ainsi permet d'éviter au maximum des conflits sur la branche de développement et simplifie les rollbacks si nécessaires. Assurez vous d'être seul à travailler sur votre branche feature (c'est le principe en même temps...). Si jamais votre tâche a mis du temps, faites des fusions régulières de la branche develop vers votre branche feature afin de travailler avec la version la plus à jour possible du projet (cela vous évite aussi les méchants merge conflicts ;)).

- Les branches "release". Ce sont les branches créées depuis celle de développement avant de faire une mise à jour de la branche de production. Le but ici est de faire une branche de test avant une de sortir une nouvelle version du projet pour le public. Depuis cette branche on effectue des tests de toutes les nouvelles fonctionnalités ainsi que des anciennes (c'est là que les tests automatiques sont utiles) et si besoin on effectue des corrections. Les commits de cette branche ne doivent servir qu'à corriger des bugs et non à finir ou rajouter des fonctionnalités (les branches features sont là pour ça). Une fois tous les bugs corrigés, cette branche est fusionnée avec la branche master puis avec la branche develop (pour récupérer les fixs). Chaque fusion d'une branche release correspond à une nouvelle version de votre projet (numérotée ainsi sous le schéma habituel MAJOR.MINOR.PATCH). Pensez aussi à mettre à jour un document de type patch note pour mettre au courant des changements liées à la nouvelle version.

- Les branches "hotfixes" (aussi appelées les branches "PANIC !!!!"). Si malgré vos nombreuses revues de code, vos corrections, vos tests automatiques et votre troisième oeil des bugs **GRAVES** se sont tout de même glissés dans la branche de production, créer une  de ces branches depuis le master et faites tout pour corriger ces bugs puis fusionner la avec le master et la branche develop. Le but de ces branches n'est pas de faire dans la finesse mais de buter des bugs critiques dans la production quitte à user de hacks bien sales. Réservez une approche plus propre pour une future branche feature. Comme les branches release, accompagner la hotfix d'une note de patch (et changer par la même occasion le numéro de la version). Ces branches ne sont à créer que dans le cas de bugs critiques qui cassent le code et foutent en l'air la production. Si jamais des bugs mineurs sont rapportés, contentez vous de créer une issue et régler la pour une prochaine release (selon son urgence).

<img alt="Schéma des branches" src="https://nvie.com/img/git-model@2x.png" height="800" />

Ces 5 types de branches constituent l'intégralité du principe de Gitflow. Elles assurent une clarté sur l'état d'un projet ainsi que sur les anciennes versions de celui-ci.

Comme dit plus haut, il s'agit d'une recommandation d'organisation. Dans certains projets plus modestes ou sur des projets de code internes, il est possible et parfois préférable de se passer de la branche de production et de n'avoir qu'une branche master à la place de la branche develop (ce qui rend par la même occasion inutile les branches release).
On peut aussi se passer des branches de releases si le projet n'est pas trop gros.

## Comment travailler sur sa branche ?

Comme décrit dans la partie précédente, chaque fonctionnalité ou tâche (une tâche pouvant consister simplement à régler des bugs) doit être développée séparémment sur sa propre branche. Ne créez pas de branche multitâches qui contiennent tout et n'importe quoi ou vous vous y perderez. Définissez bien vos tâches. Servez vous des issues pour définir les tâches et ce qui est nécessaire de développer pour les accomplir.
Une fois votre tâche accomplie, faites faire une revue du code de votre branche par un de vos collègues (de préférence un qui comprend un minimum la technologie que vous utilisez). Ce n'est qu'une fois testé et approuvé que vous ferez votre merge request vers la branche develop.

## Quelques bonnes pratiques

### Revue de code

La revue de code ne doit pas se faire à la légère. Pour faciliter votre revue, utilisez l'outil de comparaison de Git (git diff) qui est généralement intégré dans l'interface de merge request de votre hébergeur de répertoire (Gitlab ou Github). Cela vous permettra de lire uniquement les changements apportés par la branche feature.
Lors d'une revue, assurez vous des points suivants:

- Le code est-il clair ? Est-ce qu'il est bien indenté ? Il y a-t-il des commentaires et de la documentation pour décrire ce que font les algorithmes et les fonctions qui ont été ajoutées ? Si vous avez du mal à comprendre les changements, c'est probablement que le code est trop brouillon (ou bien que vous n'êtes pas la bonne personne pour faire cette revue). Notez aussi les fautes d'orthoraphe. Ce n'est pas parce qu'on est dans l'informatique qu'il faut écrire comme des sagouins.

- Le nom des méthodes et des classes ajoutées est-il compréhensible ? Ces ajouts sont-ils pertinents ? Si vous ne comprenez pas ce que fait concrétement une méthode ou une classe en se fiant à son nom ou à sa description, c'est probablement que c'est son nom est très mal choisi voire pire que la classe ou la méthode n'est pas bien conçue et fait un peu tout et n'importe quoi. N'hésitez pas à demander à abstraire le code et à le subdiviser pour plus de clarté et surtout pour que le tout soit plus "pattern-friendly". Le code dont vous faites la revue est amené à être réutilisé. S'il est irréutilisable et incompréhensible alors vaut mieux le signaler de suite. Prenez aussi garde au code en dur. C'est généralement mauvais signe.

- La tâche est-elle accomplie ? Il y a-t-il des bugs ? Testez sur votre machine la fonctionnalité ou la correction apportée par ce code. Parfois quelque-chose fonctionne sur un espace de travail et pas un autre. Demandez-vous si c'est bien comme ça que la fonctionnalité devrait fonctionner.

### Nommer ses commits

Un point très important en ce qui concerne le travail sur Git, c'est le nom donné aux commits.
Afin d'harmoniser le tout et rendre son travail clair, il vaut mieux imposer des conventions sur le nom que l'on donne aux commits.
Il y a plusieurs écoles à ce propos mais je propose tout de même les conventions suivantes:

- Des noms de commit en anglais. Oui ça peut paraitre stupide dans un groupe francophone mais si le code est amené à être lu à l'international (en particulier si on fait de l'openSource) alors il faut que les gens comprennent ce que vous avez foutu donc l'anglais est de mise.

- Des noms brefs qui résument ce qu'a fait le commit. Gardez les détails pour les patch notes. Un verbe et un COD suffisent la plupart du temps pour nommer un commit (e.g "add DataModel for Ponies").

- Des références si nécessaires. Les hébergeurs Git comme Gitlab et Github permettent de créer des références aux issues ou aux merges request dans les noms des commits. Si un commit sert à régler une issue où y est liée d'une autre façon, ajoutez une référence à celle-ci. Sur Gitlab, on peut référencer une issue en écrivant son numéro précédé d'un '#' (e.g "Fix issue #12"). On fait de même avec les merge request avec un '!'. Pour plus d'infos, cliquez [ici](https://about.gitlab.com/2016/03/08/gitlab-tutorial-its-all-connected/).

- Des préfixes ! À la manière des tags et autres labels, préfixer le nom de son commit permet de catégoriser celui-ci. Là encore il y a plusieurs écoles mais en général on encadre les préfixes avec des crochets '\[\]'. Voici une liste non-exhaustive de préfixes à utiliser avec ses commits:

    - \[WIP\] pour "Work In Progress", ce préfixe sert à signaler que le travail accompli dans le commit est encore en cours.
    - \[Fix\] sert à signaler que le commit apporte une correction de bug. Il peut être associé avec la référence de l'issue correspondante : \[Fix #12\]
    - \[Hotfix\] comme "Fix" mais pour des corrections faites en urgence dans une branche hotfix par example et donc qui est susceptible d'être modifiée plus tard.
    - \[Feat\] pour "Feature" sert à signaler que le commit apporte une nouvelle fonctionnalité.
    - \[Doc\] sert à signaler que le commit n'ajoute que de la documentation dans le code ou dans un document comme un README.
    - \[Refactor\] sert à signaler que le commit sert à réorganiser du code (déplacer des fichiers, renommer, etc).
    - \[Git\] sert à signaler que le commit opère sur des éléments propres à Git. (Changement dans le gitignore par exemple)

Libre à vous d'ajouter vos propres préfixes (du moment que les autres développeurs comprennent ce à quoi cela fait référence). Vous pouvez aussi très bien mettre plusieurs préfixes sur un seul commit.

### Faire des commits réguliers

Évitez de faire tout le code d'une feature dans un seul commit. L'intérêt des commits est de disséquer dans le temps les modifications apportées dans le code. Aussi faites des commits réguliers de votre travail (vous n'êtes pas obligés de les push systématiquement, git push poussera tous vos commits locaux d'une seule traite).
Cela simplifiera également la tâche au cas où on a besoin de rechercher l'origine d'un bug ou de faire un rollback. Dans ce cas précis, il suffira de procéder par dichotomie pour retrouver le commit qui a introduit un bug.
Pensez également à merger la branche develop avec votre branche feature pour la mettre à jour histoire de travailler avec la version la plus à jour du code et d'éviter les sempiternels conflits durant une fusion.

### Utiliser les issues

Gitlab, Github et leurs comparses disposent d'une fonctionnalité appelée "Issue". Cela permet de créer des listes de problèmes à régler. Un "problème" n'est pas forcément un bug. Ça peut surtout être un besoin qui peut être réglé par l'ajout d'une fonctionnalité.
Les issues sont pratiques pour se mettre au point sur les tâches à affectuer dans son développement et peuvent épargner l'utilisation de tableaux comme Trello (quoiqu'il est possible de synchroniser ces derniers avec vos issues).

## Résumé du workflow

Si vous êtes trop fainéants pour avoir lu tout ce qui précède cette partie (et si c'est le cas, allez le lire ! Je me suis donné du mal nom d'un chien...), voici un bref récapitulatif des étapes à suivre lors du développement de vos tâches.

### Avant de commencer une tâche

- Mettez à jour sa branche develop.
- Créez une nouvelle branche depuis la branche develop. Nommez votre branche feature. Par exemple : `feature/ceQueVousFaitesSurCetteBranche`
- Si vous avez créé votre branche localement, au premier push faites : `git push --set-upstream origin votrebranche`

### Une fois la tâche terminée

- Créez une merge request de votre branche vers la branche develop.
- Demandez une revue de votre code à un autre membre (sur Discord par exemple).
- Si le reviewer n'approuve pas, corrigez votre code jusqu'à qu'il soit validé.
- Une fois le code validé, validez la merge request, votre branche sera alors fusionnée à la branche develop.
- En cas de conflit, résolvez les à la main (depuis l'interface de Gitlab de préférence).
- Le travail terminé sur votre branche, supprimez là. Si un problème survient plus tard à cause de votre code, créez une nouvelle branche.

### À ne pas faire !

- Push sur la branche master.
- Push sur la branche develop (à moins qu'il ne s'agisse de modifications très mineures).
- Touchez à la branche de quelqu'un d'autre.
- Si jamais un bug est trouvé. Inutile d'aller chercher dans les anciens commit et de rollback le projet. On crée une nouvelle tâche, une nouvelle branche et on bosse dessus.
- Créez des branches hotfixs pour des bugs mineurs.